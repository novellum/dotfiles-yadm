
#!/bin/sh
source ~/.cache/wal/colors.sh
#B='#00000000'  # blank
B=${color2#?}
C='#ffffff22'  # clear ish
#D='#ff00ffcc'  # default
D=${color0#?}
#T='#ee00eeee'  # text
T=${color7#?}
W='#880000bb'  # wrong
#V='#bb00bbbb'  # verifying
V=${color2#?}

if [[ $XDG_SESSION_TYPE == "x11" ]]
then
i3lock \
--insidevercolor=$C   \
--ringvercolor=$V     \
\
--insidewrongcolor=$C \
--ringwrongcolor=$W   \
\
--insidecolor=$B      \
--ringcolor=$D        \
--linecolor=$B        \
--separatorcolor=$D   \
\
--verifcolor=$T        \
--wrongcolor=$T        \
--timecolor=$T        \
--datecolor=$T        \
--layoutcolor=$T      \
--keyhlcolor=$W       \
--bshlcolor=$W        \
\
--screen 1            \
--blur 5              \
--clock               \
--indicator           \
--timestr="%H:%M:%S"  \
--datestr="%A, %m %Y" \
--keylayout 2         \
else
swaylock -f \ 
	--clock \
	--indicator \
	--indicator-radius 100 \
	--indicator-thickness 7 \
    --text-color ${color0#?} \
	--ring-color ${color3#?} \
	--key-hl-color ${color2#?} \
	--line-color ${color4#?} \
	--inside-color ${color6#?} \
	-r \
    $*
fi

# --veriftext="Drinking verification can..."
# --wrongtext="Nope!"
# --textsize=20
# --modsize=10
# --timefont=comic-sans
# --datefont=monofur
# etc