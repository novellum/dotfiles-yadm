# dotfiles-yadm

Dotfiles created with the use of yadm

# arch-config

Simple and sweet install script for arch linux

# Instructions
pacman -Sy git <br />
git clone https://gitlab.com/novellum/dotfiles-yadm <br />
sh dotfiles-yadm/bin/install_pre.sh <br />
# Reminder
1 format drives <br />
2 install base <br />
3 loadkeys dvorak <br />
4 load bootloader <br />
5 generate fstab <br />

# Remember the root folder
mount /dev/sda* /mnt <br />
pacstrap /mnt sudo wpa_supplicant reflector zsh git ninja networkmanager nano base-devel fakeroot <br />
reboot <br />

# Reboot into system
systemctl enable NetworkManager <br />
git clone https://gitlab.com/novellum/dotfiles-yadm <br />
sh dotfiles-yadm/bin/install_post.sh <br />

# Final Step
su $user <br />
cd /home/$user <br />
git clone https://aur.archlinux.org/yay.git <br />
cd yay <br />
sudo -u $user makepkg -si <br />
cd .. <br />
fi <br />

yay -Sy --needed --noconfirm yadm <br />
yadm clone https://gitlab.com/novellum/dotfiles-yadm <br />
