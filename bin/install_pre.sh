# Prepare locales and keymap
echo "Prepare locales and keymap"
echo "KEYMAP=dvorak" > /etc/vconsole.conf
sed -i 's/#\(en_US.UTF-8\)/\1/' /etc/locale.gen
echo 'LANG="en_US.UTF-8"' > /etc/locale.conf

# Sync clock
hwclock --systohc
# Set date
timedatectl set-ntp true
timedatectl set-timezone America/New_York
# Generate locale
locale-gen
source /etc/locale.conf

# revert to home
cd ~

# Sort mirrors
echo "Sort mirrors"
pacman -Sy --needed --noconfirm reflector
reflector --verbose --protocol https --latest 20 --sort rate --save /etc/pacman.d/mirrorlist

pacman -Sy -needed --noconfirm wget archlinux-keyring

if ! [ -d /archfi ]; then
    git clone https://github.com/MatMoul/archfi
fi

sh ~/archfi/archfi
