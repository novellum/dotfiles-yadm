# Prepare locales and keymap
echo "Prepare locales and keymap"
echo "KEYMAP=dvorak" > /etc/vconsole.conf
sed -i 's/#\(en_US.UTF-8\)/\1/' /etc/locale.gen
echo 'LANG="en_US.UTF-8"' > /etc/locale.conf

# Sync clock
hwclock --systohc
# Set date
timedatectl set-ntp true
timedatectl set-timezone America/New_York
# Generate locale
locale-gen
source /etc/locale.conf

# Sort mirrors
echo "Sort mirrors"
pacman -Sy --needed --noconfirm reflector
reflector --verbose --protocol https --latest 20 --sort rate --save /etc/pacman.d/mirrorlist
pacman -Syu

# Configure sudo
tail -n 1 /etc/sudoers | grep targetpw || cat > /etc/sudoers <<"EOF"
Defaults:%wheel targetpw
%wheel ALL=(ALL) ALL
Defaults targetpw
EOF

# Set hostname
echo "Please enter hostname :"
read hostname
echo $hostname > /etc/hostname

# 32bit support
tail -n 1 /etc/pacman.conf | grep mirrorlist || cat >> /etc/pacman.conf <<"EOF"
[multilib]
Include = /etc/pacman.d/mirrorlist
EOF

echo "Please enter name of user :"
read user
if ! [ id -u $user ]; then
useradd -m -g users -G wheel,input,video -s /bin/zsh $user
passwd $user
fi
