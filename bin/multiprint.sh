#!/bin/sh
# yay --needed -S cups-pdf texlive-core jdk-openjdk
#default file location is /var/spool/cups-pdf/$USER
#make sure to use cups-pdf driver with options, not generic
#documentation https://www.computerhope.com/unix/ulp.htm
# http://www.it.uu.se/datordrift/maskinpark/skrivare/cups/

# needs java runtime

#/usr/bin/soffice                                                     \
#  --headless                                                         \
#  "-env:UserInstallation=file:///tmp/LibreOffice_Conversion_${USER}" \
#  --convert-to pdf:writer_pdf_Export                                 \
#  --outdir ${HOME}/lo_pdfs                                           \
#/path/to/test.docx
#'

#/usr/bin/soffice --headless "-env:UserInstallation=file:///tmp/LibreOffice_Conversion_${USER}" --convert-to pdf $file
if [ $1 = '4h' ] ; then
    suffix='4pp'
elif [ $1 = '4v' ] ; then
    suffix='4pp'
elif [ $1 = '3' ] ; then
    suffix='3pp'
elif [ $1 = 'combine' ] ; then
    suffix='combine'
    mkdir --parents $suffix
    pdfjoin --outfile $suffix/ *.pdf
    exit 1
    #gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=finished.pdf file1.pdf file2.pdf
else
    echo "Invalid input"
    echo "Valid inputs: 3, 4h, 4v"
    exit 1
fi
mkdir --parents $suffix/
for file in *
do
    name="${file%.*}"
    newname=${name// /_}
    echo "File: $name"
    if [[ "$name" = "result" || -d "${file}" || -f $suffix/"$newname"-$suffix.pdf ]] ; then
       echo "skipping existing file"
       echo ""
       continue;
    fi
    
    echo "[1/4] converting file to pdf"
    if [ $(head -c 4 "$file") = "%PDF" ] ; then
        echo "-File is already a pdf"
        cp "${file}" $suffix/src_"${newname}".pdf
    elif [[ ! -f "$name".pdf ]] ; then
        unoconv -f pdf --output=$suffix/src_"$newname" "$file"
    fi
    
    echo "[2/4] Converting pdf to new format"
    if [ $1 = '4v' ] ; then
        pdfjam --quiet --scale 0.87 --suffix $suffix --frame true --twoside --paper letter --nup 2x2 --offset '0.2in 0in' --outfile $suffix/ $suffix/src_"${newname}".pdf
        #pdfjam --quiet --scale 0.87 --suffix $suffix --frame true --twoside --paper letter --nup 2x2 --outfile $suffix/ $suffix/src_"${newname}".pdf
    elif [ $1 = '4h' ] ; then
        pdfjam --quiet --scale 0.87 --suffix $suffix --frame true --landscape --twoside --paper letter --nup 2x2 --outfile $suffix/ $suffix/src_"${newname}".pdf
    elif [ $1 = '3' ] ; then
        pdfjam --quiet --suffix $suffix --nup 1x3 --frame true --noautoscale false --scale 0.87 --offset '-3.8cm 0cm' --twoside --paper letter --outfile $suffix/ $suffix/src_"${newname}".pdf
    fi
    #pdfjam-slides3up --suffix $suffix --twoside --paper letter --outfile 4pp/ 4pp/"${newname}".pdf
    #pdfjam --quiet --suffix $suffix --nup 1x3 --frame true --noautoscale false --delta '0cm 0.2cm' --scale 0.87 --offset '-3.8cm 0cm' --twoside --paper letter --outfile 4pp/ 4pp/src_"${newname}".pdf
    
    echo "[3/4] Converting again to provide compatibility"
    gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer \ -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$suffix/"$newname"-$suffix.pdf $suffix/src_"$newname"-$suffix.pdf
    
    echo "[4/4] Removing source file"
    rm $suffix/src_"$newname".pdf
    rm $suffix/src_"$newname"-$suffix.pdf
    echo ""
done

if ! find "$suffix/" -mindepth 1 -print -quit 2>/dev/null | grep -q .; then
    echo "Nothing printed, deleting $suffix folder"
    echo ""
    rm -r $suffix/
fi

